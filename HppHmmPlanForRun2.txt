1.  Plans/ideas on next round of your analysis, 
 => Paper with full Run 2 dataset (sensitivity of current results dominated by statistical fluctuations), extension to associated H++H-/H--H+ production. 
2.  Extended/new approaches, 
 => 1) Extend to include associate production of H++H-/H--H+. 2) Based on the current analysis, explore new techniques such as charge flip tagger and prompt lepton tagger, 
       and explore feasibility of hadronic decays of the W bosons exploiting boosted signature.  
3.  Points to be addressed concerning MC modelling and theory systematics
 => These are not dominating. 
4.  Issues and ideas on statistical analysis techniques and background estimation techniques 
 => Happy with the current approaches. For the background estimation, charge flip / prompt lepton taggers will make the techniques even more robust. Different MVA approaches will be tested (require robust background estimate). 
5.  Specific needs concerning MC samples,
 => Reprocessing of the signal samples in release 21, requesting new samples for H++H-/H--H+ signal events. High statistics for WZ+jets and ttV needed to enable multivariate techniques.  
6.  Limitations due to reconstruction/calibration (CP),
 => Again, expect improvements from charge flip and prompt lepton taggers.
7.  Expected manpower and task coverage,
 => On experimental side: 3 physicists + a group of 2 students (0.3FTE*3 + 1.0 FTE *2 )
    On theoretical side: 1 postdoc as consultant.  
8.  When first look at 2017 data expected (CR’s, etc.) 
 => December 2017
9.  Plans concerning uncovered searches (see the pink section of the attached table)
 => None.
10. Other ideas?
 => None. 
