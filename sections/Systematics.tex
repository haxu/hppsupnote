
%\susection{Systematic uncertainties}

Both theoretical and experimental uncertainties can affect the search.
 The signal process and the background with prompt leptons are modeled with MC simulations. The uncertainties related to the normalizations and the acceptances  are mostly from the finite precision of the prediction of cross sections and differential kinematics, and are of theoretical nature. Experimental uncertainties 
 arise from the finite accuracy of the detector simulation and the uncertainties associated to the data-driven methods that are used to estimate the instrumental backgrounds. 

\subsection{Theoretical uncertainties}
\input{sections/theorUncert.tex} 


\subsection{Experimental uncertainties due to detector simulation and data-driven  background estimations}
There are two groups of uncertainties that affect the accuracies of the detector simulations. The first group impacts on the shapes
 of the kinematic distributions, and consequently affects the acceptance of the signal region selection.
 It includes: uncertainties on the electron energy scale and energy resolution, 
 uncertainties on the reconstruction of MET due to effects of soft tracks and uncertainties on the jet energy scale and resolutions.
 The impact on the event yields in the signal region due to these uncertainties
 is estimated by applying the same event selection on the simulated samples
 with these scales and resolutions varied by $\pm 1$ standard deviation from
 the nominal.   
  
 \iffalse
  The effects are/(should be) assessed by varying individually each source of these systematics by $\pm $ 1 standard deviation. Technically, the following variations should be implemented.

{\footnotesize \begin{verbatim} 
EG_RESOLUTION_ALL 
MET_SoftTrk_Scale 
EG_SCALE_ALL 
JET_19NP_JET_BJES_Response 
JET_19NP_JET_EffectiveNP_1 
JET_19NP_JET_EffectiveNP_2 
JET_19NP_JET_EffectiveNP_3 
JET_19NP_JET_EffectiveNP_4 
JET_19NP_JET_EffectiveNP_5
JET_19NP_JET_EffectiveNP_6restTerm 
JET_19NP_JET_EtaIntercalibration_Modelling 
JET_19NP_JET_EtaIntercalibration_NonClosure 
JET_19NP_JET_EtaIntercalibration_TotalStat
JET_19NP_JET_Flavor_Composition 
JET_19NP_JET_Flavor_Response
JET_19NP_JET_Pileup_OffsetMu 
JET_19NP_JET_Pileup_OffsetNPV 
JET_19NP_JET_Pileup_PtTerm 
JET_19NP_JET_Pileup_RhoTopology 
JET_19NP_JET_PunchThrough_MC15 
JET_19NP_JET_SingleParticle_HighPt.
\end{verbatim}
}
Currently, samples with these variation are mostly unavailable, we have implemented  these uncertainties for the SS2L signal MC samples only. These uncertainties are not concluded in the present version of the note.  
% The impact to the expected sensitivity is found to be negligible (they are all below 0.5\% and are actually pruned in the fitting). 
\fi


The other group of systematic uncertainties relevant for MC samples is the uncertainties 
on the corrections to the efficiencies of the reconstruction and selection of the final states. This group includes uncertainties on the efficiency of lepton (electron and muon) 
reconstruction and identification (labelled as "elSF" and "muSF" in the
tables), uncertainties on the trigger efficiency (referred to as "trigSF"), and jet
energy scale (19NP, labelled as "Jet") , and pile-up reweighting (labelled as
"Pileup"). The uncertainties of
 these effects are evaluated by varying the correctional factors by one standard deviation. 
Finally, the background contributions from electron charge mis-identification
 (labelled as "QMisID") and fake leptons (labelled as "Fake")  are estimated with data-driven
 techniques, and have sizeable uncertainties as described in the previous sections.
\iffalse
The list of  uncertainties included for the SS2L channnels is presented in Figure~\ref{fig:Pruning_SS2L_200}. 
\begin{figure}[htp]
  \includegraphics[width=\columnwidth]{figures/Systematics/Pruning_SS2L_200.pdf}
  \caption{The list of uncertainties included for the SS2l channel. It is a cut-and-count analysis, so all shape systematics are dropped.  The systematics are also pruned: uncertainties smaller than 0.5\% are dropped. }
  \label{fig:Pruning_SS2L_200}
\end{figure}

\begin{itemize}
\item (The uncertainty of pile-up reweighting is suspiciously large and is neglected at present.)
\item The uncertainty due to jet selection (``JVT'') is included. 
\item The uncertainty of trigger efficiency correctional scale factors for single electron triggers included, and  is referred to as ``elTrigSF'' in the figures and tables. 
\item The uncertainty of the trigger efficiency correctional scale factors for single muon triggers is decomposed to statistical and systematic components. They are labelled as ``muTrigSFStat'' and ``muTrigSFSyst'' respectively in the figures and tables.  
\item The uncertainty of the electron selection efficiency correctional scale factors is decomposed to three terms: uncertainties on the efficiencies of reconstruction (``elRecoSF''), identification (``elIDSF'') and the isolation requirment(``elIsoSF''). 
\item The uncertainty of the muon selection efficiency correctional scale factors is decomposed to muon identification ( ``muIDStat'' and ``muIDSyst'' denote the statistical and systematic uncertainties of the identification efficiencies), muon isolation requirement (``muIsoStat'' and ``muIsoSyst'' denote the statistical and systematic uncertainties of the muon isolation efficiencies) and the track-to-vertex association (``muTTVAStat'' and ``muTTVASyst'' denote the statistical and systematic uncertaties of the TTVA).
\item The electron charge mis-identification has an uncertainty of 30\%.
\item The electron fake factor has an uncertainty of 56\%.
\item The muon fake factor has an uncertainty of 51\%.
\end{itemize}
\fi


The sizes of the uncertainties in 2LSS, 3L and 4L channels are presented in the Tables~\ref{tab:SystSS2Lee} - ~\ref{tab:Syst4L} for the 200 GeV mass point. The impact of experimental uncertainties is presented for the prompt contributions and for the signal prediction. The systematics uncertainties for fake and QMisID contributions, discussed in the respective chapters, are considered as independent and are listed separately in the tables.
\begin{table}[htbp]
\begin{center}

\begin{tabular}{|c|c|c|c|c|c|}
\hline 
      & H++H--      & MisCharge      & Prompt         & Fake \\ 
\hline 

Pileup   & 0.02 & - &  0.13 &   - \\
elSF   & 0.04 & - &  0.05 &   - \\
elReso   & 0.00 & - &  0.00  &  - \\
elScal   & 0.01 & - &  0.00  &  - \\
MET   & 0.00 & - &  0.00 &   - \\
muSF   & 0.00 & - &  0.00 &   - \\
trigSF   & 0.00 & - &  0.00 &   - \\
Jet   & 0.08 & - &  0.01 &   - \\
JVT   & 0.04 & - &  0.03 &  - \\
MCnorm   & 0.00 & - &  0.19 &   - \\
QMisID   & - & 0.22 &  - &   0.16 \\
Fake   & -  &  -  &  -  &   0.23 \\
Lumi   & 0.03 & - &  0.03 &   - \\




\hline 
\end{tabular} 

\caption{Relative effect of each systematic on the yields of the SS2L ee channel, for the mass point of 200 GeV. }
\label{tab:SystSS2Lee}
\end{center}
\end{table}

\begin{table}[htbp]
\begin{center}
\begin{tabular}{|c|c|c|c|c|}
\hline
      & $H^{++}H^{--}$     & MisCharge      & Prompt      & Fake \\
\hline
Pileup   & 0.01 & - &  0.13  &  - \\
elSF   & 0.02 & - &  0.01 &   - \\
elReso   & 0.00 & - &  0.00 &   - \\
elScal   & 0.00 & - &  0.00 &  - \\
MET   & 0.00 & - &  0.00 &  - \\
muSF   & 0.01 & - &  0.01  &  - \\
trigSF   & 0.00 & - &  0.00 & - \\
Jet   & 0.03 & - &  0.36 &   - \\
JVT   & 0.03 & - &  0.04 &   - \\
MCnorm   & 0.00 & -  &  0.20  &  - \\
QMisID   & -  & 0.28 &  -  &  0.88 \\
Fake   &  -  & - &  -  &   0.81 \\
Lumi   & 0.03 & - &  0.03 &   - \\




\hline
\end{tabular}
\caption{Relative effect of each systematic on the yields of the SS2L $e\mu$ channel, for the mass point of 200 GeV}
\label{tab:SystSS2Lem}
\end{center}
\end{table}

\begin{table}[htbp]
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
      & $H^{++}H^{--}$  & Prompt      & Fake \\
\hline

Pileup   & 0.01 & 0.36 &   -  \\
elSF   & 0.00 & 0.00 &   -  \\
MET   & 0.00 & 0.05 &  -  \\
muSF   & 0.01 & 0.01 &  -  \\
trigSF   & 0.00 & 0.00  & -  \\
Jet   & 0.04 & 0.12 &   - \\
JVT   & 0.03 & 0.04 &   -  \\
MCnorm   & 0.00 & 0.19  & -  \\
Fake   & - & - &  0.56  \\
Lumi   & 0.03 & 0.03 &  -  \\



\hline
\end{tabular}
\caption{Relative effect of each systematic on the yields of the SS2L $\mu\mu$ channel, for the mass point of 200 GeV}
\label{tab:SystSS2Lmm}
\end{center}
\end{table}


\begin{table}[htbp]
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
      & $H^{++}H^{--}$    & FAKES      & Prompt \\
\hline

Pileup   & 0.00 & - &  0.01   \\
elSF   & 0.03 & - &  0.02   \\
elReso   & 0.00 & - &  0.00   \\
elScal   & 0.00 & - &  0.00   \\
MET   & 0.00 & - &  0.01   \\
muSF   & 0.01 & - &  0.01   \\
trigSF   & 0.00 & - &  0.00   \\
Jet   & 0.03 & - &  0.04  \\
JVT   & 0.03 & - &  0.04   \\
MCnorm   & 0.00 & - &  0.22   \\
Fake   & - & 0.47 &  -   \\
Lumi   & 0.03 &  - &  0.03  \\

\hline
\end{tabular}
\caption{Relative effect of each systematic on the yields of the 3L SFOS0 channel. }
\label{tab:Syst3LSFOS0}
\end{center}
\end{table}



\begin{table}[htbp]
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
      & $H^{++}H^{--}$     & FAKES      & Prompt \\
\hline
Pileup   & 0.01 & - &  0.03   \\
elSF   & 0.02 & - &  0.02   \\
elReso   & 0.00 & - &  0.00  \\
elScal   & 0.00 & - &  0.00   \\
MET   & 0.00 & - &  0.04  \\
muSF   & 0.01 & - &  0.02   \\
trigSF   & 0.00 & - &  0.00   \\
Jet   & 0.03 & - &  0.12   \\
JVT   & 0.03 & - &  0.04   \\
MCnorm   & 0.00 & - &  0.17   \\
Fake   & - & 0.51 &  -   \\
Lumi   & 0.03 & - &  0.03  \\



\hline
\end{tabular}
\caption{Relative effect of each systematic on the yields of the 3L SFOS12 channel.}
\label{tab:Syst3LSFOS12}
\end{center}
\end{table}


\begin{table}[htbp]
\begin{center}
\begin{tabular}{|c|c|c|c|}
\hline
      &  $H^{++}H^{--}$     & FAKES      & Prompt \\
\hline

Pileup   & 0.01 & - &  0.00  \\
elSF   & 0.04 & - &  0.04  \\
elReso   & 0.00 & - &  0.02  \\
elScal   & 0.00 & - &  0.02   \\
MET   & 0.00 & - &  0.02   \\
muSF   & 0.02 & - &  0.02   \\
trigSF   & 0.00 & - &  0.00   \\
Jet   & 0.01 & - &  0.17   \\
JVT   & 0.02 & - &  0.04   \\
MCnorm   & 0.00 & - &  0.35   \\
Fake   & - & 0.50 & -   \\
Lumi   & 0.03 & - &  0.03  \\



\hline
\end{tabular}
\caption{Relative effect of each systematic on the yields of the 4L channel.}
\label{tab:Syst4L}
\end{center}
\end{table}

