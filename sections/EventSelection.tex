The multi-boson final states are identified by the presence of leptons, missing transverse energy \MET and hadronic jets. To reduce the contamination from processes containing top quarks, a veto on jets tagged as containing b-quarks is applied. The event selection \footnote{The present analysis is based on the analysis infrastructure used in the ttH-multilepton analyses, including the analysis ntuples production. The description of the object selection below is reproduced from the ttH-multi-lepton ICHEP note ATL-COM-PHYS-2016-419  } is described in the following.
\subsection{Trigger}

\begin{table}[htbp]
 \begin{center}
   \begin{tabular}{|l|l|}
     \hline
                            \textbf{2015}  &  \textbf{2016}   \\
     \hline                       
HLT$\_$e26$\_$lhmedium$\_$L1EM20VH for data set &   HLT$\_$e26$\_$lhtight$\_$nod0$\_$ivarloose\\
%&  \\
HLT$\_$e60$\_$lhmedium & HLT$\_$e60$\_$lhmedium$\_$nod0  \\
HLT$\_$e120$\_$lhloose & HLT$\_$e140$\_$lhloose$\_$nod0 \\
HLT$\_$mu20$\_$iloose$\_$L1MU15 &   HLT$\_$mu26$\_$ivarmedium\\
% (after ICHEP) \\
HLT$\_$mu50 & HLT$\_$mu50 \\
     \hline
   \end{tabular}
   \caption{\label{tab:triggers} Summary of triggers used by data taking period. } 
 \end{center}
\end{table}
The data used in this analysis have been collected by unprescaled single lepton triggers, described in Table~\ref{tab:triggers}. In each period, the above triggers are used with a logical OR. All samples and selections mentioned in this paper include a requirement that at least one of the leptons be trigger matched and have a transverse momentum above $30$~GeV, in order to ensure a high trigger efficiency across the analysis phase space. 

\subsection{Object definition }
The following objects reconstructed in the detector are used in the analysis.
\subsubsection{Leptons}
In this analysis only light leptons (electrons and muons) are used.
\iffalse
\paragraph{Electrons}
Electrons are reconstructed as tracks in the inner detector and clusters in the electromagnetic calorimeter,  within the region of pseudo-rapidity $|\eta| < $ 2.47. The candidate in crack region  1.37 $ < |\eta| < $ 1.52 is removed. Only those electron candidates with transverse momentum greater than 10 GeV
are considered. A likelihood-based discriminant is built on shower shapes in electromagnetic calorimeter and
track qualities from inner detector. It is used to separate electrons from fakes mainly coming from hadron 
decays and photon conversion. The \textit{looseAndBLayer} working point is used which gives approximate $~$95\% 
electron efficiency~\cite{Anastopoulos:2142831}. 
To further suppress hadronic background, electron candidate is required to be isolated. Two isolation variables
are computed: $E^{cone20}_{T}/p_{T}$~\footnote{$E^{cone20}_{T}$ in this note means that, the isolation variable 
being reconstructed from topological clusters.} 
based on the energy of calorimeter topological clusters in a cone of 
$\Delta$ R $<$ 0.2 around the electron candidate (with energy from itself being subtracted) divided by its 
transverse momentum, and $p^{varcone20}_{T}/p_{T}$, the use of 
scaler sum of all track momentum in a cone of $\Delta$ R $<$ 0.2 (with excluding the tracks associated to itself) 
divided by its transverse momentum. Flat efficiencies of 99\% in $\eta-p_{T}$ plane are achieved for electrons
by applying cuts on those two isolation variables. The longitudinal impact parameter of the electron
track with respect to the selected event primary vertex, multiplied by the sine of the polar angle, 
$|z_{0}sin\theta|$, is required to be less than 0.5 mm. The transverse impact parameter divided by the 
estimated uncertainty on its measurement, $|d_{0}/\sigma(d_{0})|$, must be less than 5. 

\paragraph{Muons}
 Muon candidate is reconstructed by combining tracks formed in inner detector and in muon spectrometer, within 
the region of pseudo-rapidity $|\eta| < $ 2.5. Only those muon candidates with transverse momentum greater than 
10 GeV are considered. Muon candidate has to pass \textit{loose} quality and to pass track requirements. 
To further suppress hadronic background, muon candidate is required to be isolated. Two isolation variables
are computed: $E^{cone20}_{T}/p_{T}$ based on the energy of calorimeter topological clusters in a cone of 
$\Delta$ R $<$ 0.2 around the muon candidate (with energy from itself being subtracted) divided by its transverse 
momentum, and $p^{varcone30}_{T}/p_{T}$, the use of scaler sum of  
all tracks momentum in a cone of $\Delta$ R $<$ 0.2 (with excluding the track associated to itself) divided
by its transverse momentum. Flat efficiency of 99\% in $\eta-p_{T}$ plane are achieved for muons
by applying cuts on those two isolation variables. The longitudinal impact parameter of the muon
track with respect to the selected event primary vertex, multiplied by the sine of the polar angle, 
$|z_{0}sin\theta|$, is required to be less than 0.5 mm. The transverse impact parameter divided by the 
estimated uncertainty on its measurement, $|d_{0}/\sigma(d_{0})|$, must be less than 3. 

\paragraph{Leptons selection strategy}
\fi

%The lepton identification is tuned such that an optimal efficiency versus background rejection is achieved.  
The lepton selection uses the official working points in identification algorithms and isolation. These are combined with impact parameters with respect to the primary vertex (PV), transverse and longitudinal to the beam-line, in order to define two types of lepton requirement : tight and loose.
The signal search for 2LSS and 3L channels is performed using the  "tight" lepton definition (for both leptons in 2LSS and for the two same sign leptons in the 3L channel), for which the tight identification algorithms and restrictive isolation conditions are applied. In order to study the contributions from hadrons or photons misidentified as leptons ("fake leptons"), the "loose" identification setup is used, with loose identification selection and relaxed isolation conditions. Because of favourable background conditions, the "Loose" selection is used in the 4L channel. The conditions for loose and tight definitions for electrons and muons are summarised in Table~\ref{Tab:loosetight}.

\begin{table}[htbp]
 \begin{center}
   \begin{tabular}{|c||cc|cc|}
     \hline
Lepton    & \multicolumn{2}{c|}{Electrons} &      \multicolumn{2}{c|}{Muons} \\
     \hline   
Condition           & Loose & Tight & Loose & Tight \\                    
     \hline
  		$P_T$ &             \multicolumn{2}{c|}{$P^e_T > 10$~GeV } &        \multicolumn{2}{c|}{$P^e_T > 10$~GeV } \\
		Pseudo-rapidity &	    \multicolumn{2}{c|}{$|\eta_e|<2.47$ , not in crack $1.37:1.52$ } &  \multicolumn{2}{c|}{$|\eta_\mu|<2.5$ } \\
Identification  &	 {\small  LooseLH }&    {\small TightLH} &  {\small Loose} &   {\small Loose } \\
Isolation &          {\small  Loose }&   {\small FixedCutTight }&   {\small LooseTrackOnly} & {\small FixedCutTightTrackOnly} \\
PV longitudinal &   $|z_{0}sin\theta|<0.5$ mm &   $|z_{0}sin\theta|<0.5$ mm&   $|z_{0}sin\theta|<0.5$ mm &   $|z_{0}sin\theta|<0.5$ mm \\
PV transverse  &   $|d_{0}/\sigma(d_{0})| < 5$ &    $|d_{0}/\sigma(d_{0})| < 5$  &   $|d_{0}/\sigma(d_{0})| < 3$ &    $|d_{0}/\sigma(d_{0})| < 3$  \\
     \hline
   \end{tabular}
   \caption{\label{Tab:loosetight} Summary of electron and muons selection conditions used in the analysis.} 
 \end{center}
\end{table}


\subsubsection {Jet object selection} \label{sec:jets}
Jets are reconstructed from topological clusters using the anti-k$_\mathrm{T}$ algorithm with a radius parameter of $R = 0.4$. Only jets with $p_T > 25$~GeV and $|\eta| < 2.5$ are considered. For jets with $p_T < 60$~GeV and $|\eta| < 2.4$, the output of the jet vertex tagger - JVT - 
%is required to be larger than 0.64(0.59) in 2015 (2016) data, in order to
is required to be larger than 0.59, in order to
suppress jets originating from pile-up. Jets are discarded if they overlap with leptons, as explained below. Jets containing $b$-hadrons are identified ($b$-tagged) via a multivariate discriminant combining information from the impact parameters of displaced tracks with topological properties of
secondary and tertiary decay vertices reconstructed within the jet (algorithm $MV2c10$, with a working point of $70\%$). The working point used for this search corresponds to an average efficiency of 70\% for $b$-jets with $\pt > 20$~GeV and $|\eta| < 2.5$ in $t\bar t$ events. The expected rejection factors against light and $c$-jets are 380 and 12, respectively.
%Selected jets that overlap with baseline electrons (see above) within
 %$\Delta R < 0.2$ are discarded. Selected jets with less than three tracks that overlap with baseline muons within  $\Delta R < 0.4$ are also discarded.


\subsubsection{ Missing transverse energy} 
 
The missing transverse momentum (\MET or $MET$ in the following) in the events is defined as the transverse momentum imbalance
  in the detector. It is calculated as the negative vector sum of the momentum of user-selected calibrated  objects (electrons, muons, jets), and of the soft-event contribution, which is reconstructed from tracks or calorimeter cell clusters not associated with the hard objects. In the presented analysis, \MET is reconstructed with the METMakerTool, using calibrated electrons and  muons passing the baseline selection, and calibrated jets before any selection (full jet container). Pile-up  degrades the resolution of the calorimeter-based measurement of the soft term. Following the Jet/\MET group recommendations   the track-based measurement of the soft term is used in this analysis.
 % (\begin{verbatim} https://twiki.cern.ch/twiki/bin/view/AtlasProtected/JetEtmissRecommendations2016  \end{verbatim})
%\textit{The \MET reconstruction has been cross-checked with simplified calculations using hard analysis objects. The \MET and $m_W$ resolution performance has been compared for these cases. These studies are detailed in Appendix XXXXX.}


\subsubsection{Overlap Removal}
\label{OvR}
In absence of a coherent particle flow algorithm implemented at reconstruction level, an ad-hoc overlap removal is performed among objects, on top of above baseline selection.
A detailed explanation has been documented in ~\cite{Adams:1743654}. A short overview
is given in this paragraph.
The removal procedure\footnote{https://indico.cern.ch/event/539619/contributions/2191033/attachments/1284239/1909241/Farrell\_ort\_asg.pdf}  is summarised in Table ~\ref{Tab:overlap-removal}.
%Tau is removed if it closes to any electrons or muons within $\Delta R < $ 0.2.
%The electron is removed if it shares track with a muon. A jet is removed
%if it is close to any electron within $\Delta R < $ 0.2. Then any electron is removed if it is close to any remaining  jet
%within $\Delta R < min( 0.4, 0.04 + 10/ P_T \mathrm{[GeV]} )$. Furthermore, a jet is removed if it is close to a muon within $\Delta R < min( 0.4, 0.04 + 10/ P_T \mathrm{[GeV]} )$ and is associated with less than three tracks, otherwise the respective muon is removed.
%%Finally, jet is removed if it closes to any tau within $\Delta R < $ 0.2. 
%(note that if the overlap removal change for the full data set processing/ntuples, the description have to be updated)
Any electron candidate within $\Delta R = 0.1$ of another electron candidate with a higher $p_{T}$ is removed. Any electron candidate within $\Delta R = 0.1$ of a muon candidate 
is also removed. Any jet within $\Delta R = 0.3$ of an electron candidate is removed. If a jet and a muon candidate lie within 
$\Delta R = min( 0.4, 0.04 + 10/ P_T(\text{muon}) \mathrm{[GeV]} $ of each other, the jet is kept while the muon is removed. 


\begin{table}[htbp]
 \begin{center}
   \begin{tabular}{|c|c|c|}
     \hline
                            \textbf{Keep}  &  \textbf{Remove} & \textbf{Cone size ($\Delta$ R) or track}  \\
     \hline                       
                             electron    & electron (low $p_T$) & 0.1\\
     \hline
%                            electron    & tau   & 0.2 \\
 %    \hline
%                            muon        & tau   & 0.2 \\
%     \hline 
%                            electron    & CaloTagged muon & shared track \\
%     \hline                       
%                            muon        & electon & shared track \\
                            muon        & electon & 0.1 \\
     \hline                       
                            electron    & jet   & 0.3 \\
%     \hline                       
%                            jet         & electron & $\Delta R < min( 0.4, 0.04 + 10/ P_T \mathrm{[GeV]} )$ \\
%     \hline                       
%                            muon        & jet   & (0.2 or ghost-matched to muon) and (numJetTrk $\leq$2) \\     
     \hline                 
                            jet         & muon  & $\Delta R < min( 0.4, 0.04 + 10/ P_T(\text{muon}) \mathrm{[GeV]} )$\\
     \hline
 %                           tau         & jet   & 0.2 \\        
 %    \hline
   \end{tabular}
   \caption{\label{Tab:overlap-removal} Summary of overlap removal between electrons, muons and jets. The tau hadronic decays are not treated in this event final state decomposition and are part of the hadronic final state (included in jets reconstruction).} 
 \end{center}
\end{table}
\subsection{Signal and background event topologies}
The searched signal contains four $W^\pm$ bosons, two (or less) of which decays to dijets for the 2LSS (3L,4L) analysis channels. The event topologies are illustrated in Figure~\ref{Fig:SignalTopo}. The signal event of 2LSS and 3L channels contains therefore a significant number of jets with high transverse momentum, with a potential resonant mass distribution around the $W$ mass. In addition,  the same-sign di-leptons in 2LSS, 3L and 4L topologies originate from the same scalar boson $H^{\pm\pm}$~and therefore tend  to be close in $\eta-\phi$, while the remaining jets for 2LSS, and the opposite sign lepton for the 3L case, are generally produced  at larger distance in $\eta-\phi$ from the same-sign di-leptons. The jets originating from $W$'s are in general originating from light quarks $udsc$, leading therefore to events with no $b$-jets. 
\begin{figure}
\subfloat[2LSS  \label{fig:Topo2LSS}]{
  \includegraphics[width=.32\linewidth]{figures/pheno/2LSS.png}
  }
\hfill
\subfloat[3L\label{fig:Topo3L}]{
  \includegraphics[width=.32\linewidth]{figures/pheno/3L.png}
  }
\hfill
\subfloat[4L\label{fig:Topo4L}]{
  \includegraphics[width=.32\linewidth]{figures/pheno/4L.png}
  }

\caption{Illustrations of event topologies of the signal process.}
\label{Fig:SignalTopo}
\end{figure}


The analysis channels (2LSS, 3L and 4L) face various background contributions from the SM. The 2LSS topology is populated with events containing one true lepton (from $W$, and to some lesser extent from $Z$ bosons) and one fake lepton from the hadronic final state. The 2LSS events with electrons can also originate from the  opposite sign pairs originating from Drell-Yan and $t\bar{t}$ production, where the charge of one of the electron is mis-identified (the charge misidentification rate is negligible for muons). For 3L, a significant part of the expected SM contribution originate from di-boson production. In particular, the $WZ$ production, where both bosons decay to leptons, presents also other signal features like significant \MET and absence of b-jets for most of the production cross section. It should be noted that the same-flavour opposite sign lepton pairs are predominantly produced in the $Z$ bosons mass window. One noticeable feature of the  $t\bar{t}$ background  (and in general of processes containing at least one top quark) is the presence of b-jets, identifiable using multivariate algorithms relying on the lifetime properties of the $b$-hadrons  contained in jets.  

Besides lepton and jet identification and kinematics quantities,  several event level variables are defined to exploit the signal features described above:
\begin{itemize}
\item $M^W_{jj}$: the invariant mass of two jets closest to the W-boson mass. 
%Alternatively $M_{\mathrm{jets}}$ is used for 2LSS channel to reconstruct the hadronically decaying $H^{\pm\pm}$ (in 4 jets).  
\item $M_\mathrm{jets}$: the invariant mass of all jets in the event. When there are more than 4 jets in the event, only the leading 4 jets are considered for the calculation of the invariant mass.  
\item $M_{\ell_i\ell_j}$: di-lepton invariant mass. While a complete reconstruction of the $H^{\pm\pm}$ bosons is not possible using leptons due to escaping neutrinos, the variable still reflects the mass scale of the boson.
\item $\Delta R_{\ell_i\ell_j}$: the distance in $\eta-\phi$ between two same-sign leptons. The same sign leptons tend to close for the signal, while some part the background favours a back-to-back topology.  In $4L$ channel, two such variables can be calculated per event, and the minimal and maximal value are used to discriminate the signal from the background. This and the previous variables can be used for any lepton pair in all analysis channels.
\item $\Delta\phi(\ell\ell, MET)$: difference in azimuth between the di-lepton system and the $\MET$. This variable is used in the 2LSS analysis. 
\item $RMS$: this is a variable used by the 2LSS channel to describe the ``spreads'' of the azimuthal angles of leptons, $\MET$, and jets, defined as the R.M.S of the $\phi$'s of the leptons and $\MET$ times that of the jets divided by the R.M.S. of the $\phi$'s of leptons, $\MET$, and jets:
\begin{equation}
\mathrm{RMS} = \frac{\mathrm{R.M.S.}(\phi_{\ell_1},\phi_{\ell_2},\phi_{\MET})*\mathrm{R.M.S.}(\phi_{j1},\phi_{j2},\cdots  )}{\mathrm{R.M.S.}(\phi_{\ell_1,},\phi_{\ell_2},\phi_{\MET},\phi_{j1},\phi_{j2},\cdots) }.
\end{equation}
In the 2LSS channel, the W bosons originating from one Higgs particle should decay to charged leptons and neutrinos, and the W bosons from the other Higgs particle should both decay to hadronic jets. Due to the spin correlations,  the two charged leptons tend to be close in the azimuthal plane. The directions of MET and leptons should be centered around the direction of the Higgs particle. So smaller spread in the azimuthal angles of leptons in the events are expected.  The same correlations are expected for the jets from the other Higgs particle. The ratio of the R.M.S defined above characterizes the feature of signal events.
\item $\Delta R_{\ell_i-jet}$: the minimal distance in eta-phi between a lepton and leading or sub-leading jet. 
\item $\Delta \Phi_\mathrm{MET-jet}$: the distance in azimuth between the MET and the leading jet.
\item $M_{3\ell}$: the invariant mass of  three leptons (for the 3L channel).
\item $M_{4\ell}$: the invariant mass of  four leptons (for the 4L channel).
\end{itemize}
%After the common description about the object defs and event topology 
%split to 2 channels

%\subsection{Selection in two same sign leptons channel}
%(to be inserted)

