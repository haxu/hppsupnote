Form EB V  0.001  August 8th, 2016


- Analysis team (can be imported from CDS of support note): 
Cristinel Diaconu [1]
Venugopal Ellajosyula[1]
Yanwen Liu[2]
Emmanuel Monnier[1]
Ruiqi Zhang [1,2]
[1] CPPM, Aix-Marseille Universite, Marseille, France.
[2] University of Science and Technology of China, Hefei, China.

- Proposed contact editors:
Cristinel DIACONU, Yanwen LIU

- Data set used:
2015 and 2016 pp, 25 ns; baseline discussion with the EB will be based on 13.2 fb -1, the luminosity will be updated before final release. 

- Supporting notes:
https://cds.cern.ch/record/2207603

- One paragraph summary (physics message, techniques used, etc.):
This is a novel search for doubly charged Higgs bosons using the di-boson decay channel, never used before. We have developped the model together with a group a theorists, some of which will be included as external collaborators. The model is a "see-saw type II" incorporating salient features like natural explanation of neutrino masses and a reserved place for teh SM higgs boson in the extended scalar sector phenomenology. Experimentally, the studied final states include same-sign dileptons and trileptons, with multiple jets and MET in a cut-based analysis. The contributions from Z leptonic decaus are supressed by a mass window veto and events from ttbar are supressed by rejecting events with tagged b-jets. Angular correlations between leptons and jets are used in the last steps of the analysis to further reject teh SM background. 

- Previous ATLAS publication(s) (include Glance link) and why this result is an improvement:
The model has not been explored before. It is developped by teh analysers in common with theorists from Unievrsity of Montpellier (Gilbert Moultaka) for whom an association to ATLAS will be requested.

- Related ATLAS analyses and why this one is different enough to justify a separate paper:
The signature (multileptons, MET, jets, b-veto) is not included in other exotic searches. The H++ search via same-sign di-leptons only is not sensitive to the proposed model.

- Abstract:
A search for doubly charged scale bosons decaying to charged vector bosons is performed using a data sample corresponding to an integrated luminosity of 11.7~fb$^mathrm{-1}$ (more luminosity will be included for the public result) collected by the ATLAS detector at LHC at a centre-of-mass energy of 13 TeV.  The search is guided by a model including an extension of the scalar sector though an scalar triplet, leading to a rich phenomenology that include singly- and doubly-charged bosons, two new pseudo-scalar bosons and  a scalar boson associated to the SM Higgs boson. The doubly-charged bosons $H^\pm\pm$ are produced either by pairs, or in association with a single charged boson.  Two scenarios are explored: exclusive, with only $H^\pm\pm$ bosons in the observable mass range and all other new scalars  at higher masses, and "degenerate", when all new bosons have very close masses. The search focusses in the multi-bosonic decays $H^\pm\pm\rightarrow W^\pm\pm$. The mass range from 200 to 700 GeV is explored using two experimental signatures: the same sign di-leptons with missing transverse energy and more than four jets, and tri-leptons with missing transverse energy and at least two jets, where events with dileptons in the $Z$-boson mass range or containing tagged $b$-jets are vetoed.  

- Target date for the paper:
September 30, 2016

- Potential EB chair:
Emmanuel Sauvan, Peter Onyisi, Borut Kersevan

- Potential EB members:
Rustem Ospanov(ttH, 2LSS), Peter Onysi(ttH), JunJie Zhu, Louis Helary, Emmanuel Sauvan (WZ), Jacobo Searcy, Yusheng Wu, Tamara Vazquez Schroeder (ttH), Borut Kersevan (dileptons)
